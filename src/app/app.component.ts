import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  message = 'Hello, Angular! :D'
  private changed = false

  changeMessage() {
    this.message = this.changed ? 'Hello, Angular! :D' : 'Welcome to Angular'
    this.changed = !this.changed
  }
}